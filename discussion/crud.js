let http = require("http");

//Mock database
let directory = [
	{
		"name": "Rendon",
		"email": "rendon@email.com",
	},
	{
		"name": "Jobert",
		"email": "jobert@email.com" 
	},
	{
		"name": "CongTV",
		"email": "congtv@email.com" 
	},
	{
		"name": "Pingris",
		"email": "pingris@email.com" 
	}
];

http.createServer(function(request, response){

	if(request.url == "/users" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'application/json'})

		//JSON.stringfy() - Transform a javascript into JSON String
		response.write(JSON.stringify(directory));
		response.end();
	}

	//Route for creating a new item upong receiving a POST request
	// A request object contains several parts: 
		//Headers - Contains information about the request context/content like what is the data type
		//Body - contain the actual information being send with the request.
		// NOTE: the request body can  NOT be empty 
	else if(request.url == "/users" && request.method == "POST"){

		//Declaring initialize a "requestBody" variable to an empty string
		//This will act as a placeholder for resource/data to be created later on.

		let requestBody = '';

		// A stream is squence of data
		// Data is receive from the client and is processed in the 'data' stream
		// The information provided from the request object enters a sequence called "data" the code below will be trigger
		// Data step - this reads the "data" stream and processes it as the request body. 
		// on method binds an event to a object
		// It is a way to express your intent if there is something happening (data sent or error in your  case), then execute the funtion added as a parameter. This style of programming is called Event-driven programming
		// event-driven-programming is a programming paradigm in which the flow of the program is determined by events such as user action ( mouse clicks, key presses) or message passing from other programms or threds.


		request.on('data', function(data){

			requestBody += data
		});

		request.on('end', function(){

			//Check if at this point the requestBody is of data tye STRING
			//We need this to be of data type JSON to access its property.

			console.log(typeof requestBody);

			//JSON.pase() - Takes a JSON string and transform it into javascript object
			requestBody = JSON.parse(requestBody);

			//create a new object representing the new mock database record 

			let newUser = {
				"name" : requestBody.name,
				"email": requestBody.email
			}

			//Add the user into the moch database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application.json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
}).listen(4000);
console.log('Server running at localhost:4000');

	