/*Activity:
1. In the S27 folder, create an activity folder and an index.js file inside of it.
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
- If the url is http://localhost:4000/, send a response Welcome to Booking System
- If the url is http://localhost:4000/profile, send a response Welcome to your profile!
- If the url is http://localhost:4000/courses, send a response Here’s our courses available
- If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
- If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
- If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
3. Test all the endpoints in Postman.
4. Create a git repository named S27.
5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
6. Add the link in Boodle.

*/

let http = require("http");


const server = http.createServer(function (request, response) {


	if(request.url == "/" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to Booking System");
	}

	else if (request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to your profile!");
	}
	else if (request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Here’s our courses available");
	}
	else if (request.url == "/addCourse" && request.method == "POST"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Add a course to our resources");
	}
	else if (request.url == "/updateCourse" && request.method == "PUT"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Update a course to our resources");
	}
	else if (request.url == "/archiveCourse" && request.method == "DELETE"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Archive courses to our resources");
	}


}).listen(4000);

console.log('Server is running at localhost:4000');